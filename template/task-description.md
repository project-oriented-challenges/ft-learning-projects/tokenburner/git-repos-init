## Task description

### Introduction

#### Decentralized application (DApp)

Decentralized application is a form how applications are going to be executed in the WEB3 paradigm. Shortly, the general idea is that the application will not be hosted on some concrete system in order to reduce possibility possibility of censorship or downtime. Instead it is assumed that anyone who would like get the access to a specific service can run the application. The front-end of the application is initially located on git servers -- one need to clone it and run locally (probably in a containerized environment). The back-end of the application is a set of smart contracts in the blockchain.

The applications are being developed as so they authorize the user actions in the blockchain by accessing the private keys through different mechanisms. For example, applications developed for popular web browsers can use [the MetaMask extension](https://metamask.io/). 

#### EIPs

Similar to well known RFC (request for comments) -- a way how to governance of the Internet development has been organized, [EIPs (Ethereum Improvement Proposals)](https://eips.ethereum.org/) were introduced. 

They _describe standards for the Ethereum platform, including core protocol specifications, client APIs, and contract standards_.

On earlier stages the same documents were called _Ethereum Request for Comments -- ERC._ But it was assumed that their focus lies more on application-level standards for Ethereum and can include token standards, name registries, library/package formats, and more.

#### ERC20 token standard 

[ERC20](https://eips.ethereum.org/EIPS/eip-20) is one of the first ERC/EIP which was designed and widely used. The popular block explorer Etherscan [reports](https://etherscan.io/tokens) about more than `240'000` of the ERC20-compatible contracts deployed to the Ethereum Mainnet.

The idea of the contract is the following: to allow blockchain projects to introduce a fungible entity instead of deploying their own sidechain. Introduction of such token contracts brought an enormous adoption to the Ethereum blockchain. The tokens could play the role of [utility](https://en.bitcoinwiki.org/wiki/Utility_token) (examples: tickets, loyalty points), [security](https://en.bitcoinwiki.org/wiki/Token#Security_token) (examples: shares, futures) or combined.

The basic operations that any ERC20-compatible token contract provides is:
  * to transfer some amount of tokens from tokens owner to another account (changing of the ownership)
  * to get balance of some account -- amount of tokens owned by the account

You can get familiar with a basic token contract by reading the code of [the following token](https://blockscout.com/poa/sokol/tokens/0x968dF7b2114e095F9b9cF039de5E35CDe7AF095C) deployed to the Sokol Testnet.

### Task statement

Prepare a web-based Dapp which will allow to perform very simple actions with an ERC20-compatible token:
  * get the balance
  * burn tokens (make impossible further changing of ownership for some amount of tokens)

### Pre-requisites

1. The application is run by invoking `docker-compose up --build`

2. After the application running the invocation of `docker ps -a` displays at least one container with the name `web`. The container exposes the port 8000 as so all requests sent to this port will be forwarded on the port 80 within the container. So, the application will be available from the machine hosting the container by URL `http://localhost:8000/`.

3. When the Dapp's page is opened in the browser it requests the access to [the MetaMask extension](https://metamask.io/).

4. When the access to the wallet is granted, the following us displayed on the page:
  * (1) an input field (`id="contractAddress"`) to enter an address of an ERC20 token contract;
  * (2) a button (`id="getBalance"`) to get the ERC20 token balance of the current user account;
  * (3) a text area (`id="balance"`) to display the requested balance, contains `N/A` by default;
  * (4) an input field (`id="countToBurn"`) to define the ERC20 token value;
  * (5) a button (`id="burnButton"`) to trigger the token burning.

### Requirements 

#### US-01 -- Getting the ERC20 token balance

The application user is able to request the balance of an ERC20 token by specifying the token contract address.

_Acceptance criteria:_

_**AC-0101**_

Actions:
1. Enter an ERC20 token contract address to the input field (1) and press the button (2).
2. Change the account in MetaMask and press the button (2) on the application page.

Expected results:
1. The text area (3) displays the token balance of the account which is selected in the the MetaMask. The balance is outputted as `x.y`, where `x` -- is a decimal part and `y` -- is a fractional part of the balance after its conversion by dividing the balance by `10` in power of the value extracted from the contracts field `decimals`. The precision of the output value is 6 signs after the dot. E.g. if the balance is `2500000000000000000` and the method `decimals()` returns `18`, the value in the text field is `2.500000`; if the balance is `123456789` and `decimals()` returns `9`, the value `0.123456` is displayed; if the balance is `0`, the output is `0`. The pause between the button pressure and the text area update is not greater than 2 seconds.
2. The text area (3) updated with the token balance of the new account.

_**AC-0102**_

Actions:
1. Enter an address of a contract is not compatible with the ERC20 standard to the field (1) and press the button (2).

Expected results:
1. The text area (3) displays `N/A`. The alarm window with the message "Specify an ERC20 token contract address" appears.

_**AC-0103**_

Actions:
1. Enter an address which is not associated with any contract to the field (1) and press the button (2).

Expected results:
1. The text area (3) displays `N/A`. The alarm window with the message "Specify an ERC20 token contract address" appears.

_**AC-0104**_

Actions:
1. Enter an invalid address to the field (1) and press the button (2). The invalid address is defined as a string which length differs from `42` or does not contain `0x` at the beginning or contains other symbols besides hexadecimal digits.

Expected results:
1. The text area (3) displays `N/A`. The alarm window with the message "Specify an ERC20 token contract address" appears.

_**AC-0105**_

Actions:
1. Leave the field (1) empty and press the button (2).

Expected results:
1. The text area (3) displays `N/A`. The alarm window with the message "Specify an ERC20 token contract address" appears.

#### US-02 -- Burning tokens

The application user is able to burn some amount of tokens they own.

**Note:** "burning the tokens" assumes that the corresponding amount of tokens is sent to `address(0)`.

_Accpetance criteria:_

_**AC-0201**_

Actions:
1. Choose in MetaMask an account that owns enough tokens in an ERC20 token contract, enter the ERC20 token contract address to the input field (1), enter some amount of tokens in the input field (4) and press the button (5). The amount of tokens should be entered in the same format as used to display the account balance: `x.y`, where `x` -- is a decimal part and `y` -- is a fractional part. They are converted to the actual amount of tokens by multiplying the entered value by `10` in power of the value extracted from the contracts field `decimals`. E.g. if the entered value is '2.5' and the method `decimals()` returns `18` the amount to burn is `2500000000000000000`; if the value is `0.123456` and `decimals()` returns `9`, the amount of tokens to burn is `123456000`.
2. Choose another account in MetaMask, enter the ERC20 token contract address to the input field (1), enter some amount of tokens in the input field (4) and press the button (5).

Expected results:
1. The MetaMask window appears with a request to the user to sign a transaction. After the transaction inclusion into a new block, checking of the contract state in the blockchain shows that the token balance of the account originated the transfer is reduced by amount of tokens to burn. The text area (3) can contain any value.
2. The MetaMask window appears with a request to the user to sign a transaction. After the transaction application, checking of the contract state in the blockchain shows that the token balance of the account originated the second transfer is reduced by amount of tokens to burn. The text area (3) can contain any value.

_**AC-0202**_

Actions:
1. Choose in MetaMask an account that owns some (non zero) tokens in an ERC20 token contract, enter the ERC20 token contract address to the input field (1), enter the amount of tokens which is larger than the balance of the account in the input field (4) and press the button (5).

Expected results:
1. The alarm window with the message "Requested number of tokens is too big" appears

_**AC-0203**_

Actions:
1. Choose in MetaMask an account that owns some tokens in an ERC20 token contract, enter the ERC20 token contract address to the input field (1), enter zero to the input field (4) and press the button (5).

Expected results:
1. The alarm window with the message "Requested number of tokens cannot be zero" appears

_**AC-0204**_

Actions:
1. Choose in MetaMask an account that owns some tokens in an ERC20 token contract, enter the ERC20 token contract address to the input field (1), enter some symbols (besides `0-9` and the delimiter) to the input field (4) and press the button (5).

Expected results:
1. The alarm window with the message "Put a number as the amount to burn" appears

_**AC-0205**_

Actions:
1. Choose in MetaMask an account that owns some tokens in an ERC20 token contract, enter the ERC20 token contract address to the input field (1), leave the input field (4) empty and press the button (5).

Expected results:
1. The alarm window with the message "Put a number as the amount to burn" appears

_**AC-0206**_

Actions:
1. Choose in MetaMask an account that owns some tokens in an ERC20 token contract, enter an address of a contract is not compatible with the ERC20 standard to the input field (1), enter some amount of tokens to the input field (4) and press the button (5).

Expected results:
1. The alarm window with the message "Specify an ERC20 token contract address" appears

_**AC-0207**_

Actions:
1. Choose in MetaMask an account that owns some tokens in an ERC20 token contract, enter an address which is not associated with any contract to the input field (1), enter some amount of tokens to the input field (4) and press the button (5).

Expected results:
1. The alarm window with the message "Specify an ERC20 token contract address" appears

_**AC-0208**_

Actions:
1. Choose in MetaMask an account that owns some tokens in an ERC20 token contract, enter an invalid address in the input field (1) empty, enter some amount of tokens to the input field (4) and press the button (5).

Expected results:
1. The alarm window with the message "Specify an ERC20 token contract address" appears

_**AC-0209**_

Actions:
1. Choose in MetaMask an account that owns some tokens in an ERC20 token contract, leave the input field (1) empty, enter some amount of tokens to the input field (4) and press the button (5).

Expected results:
1. The alarm window with the message "Specify an ERC20 token contract address" appears
