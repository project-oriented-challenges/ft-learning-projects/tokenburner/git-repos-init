#!/usr/bin/env python

import csv

# The script expects the csv file that keeps informaton about repos in form of
# <group-of-participants>;<group-of-projects>

META_GROUP="onti-fintech/onti-2020-fintech/"
#META_GROUP=""

exec_file = "#!/bin/bash\n\n"

with open('repos.csv') as csvfile:
    d = csv.reader(csvfile, delimiter=";")
    for row in d:
        print(row)
        gr   = row[0]
        i    = row[1]
        exec_file+=f'echo "generate repo for {i}"\n'
        exec_file+="cp -a template repo\n"
        exec_file+="cd repo\n"
        exec_file+="git init\n"
        exec_file+=f'git remote add origin git@gitlab.com:alex.kolotov/test-and-remove.git\n'
        # if len(META_GROUP) != 0:
        #     exec_file+=f'git remote add origin git@gitlab.com:{META_GROUP}{gr}/{i}/tokenburner.git\n'
        # else:
        #     exec_file+=f'git remote add origin git@gitlab.com:{gr}/{i}/tokenburner.git\n'
        exec_file+="git add .\n"
        exec_file+="git commit -m 'Initial commit from template [skip ci]'\n"
        exec_file+="git push -u origin master\n"
        exec_file+="cd ..\n"
        exec_file+="rm -rf repo\n\n"
    
with open('init-repos.sh', 'w') as script_file:
    script_file.write(exec_file)
